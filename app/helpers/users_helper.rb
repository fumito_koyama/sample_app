module UsersHelper

  def gravatar_for(user,size:80)#キーワード引数　メソッド(名前:引数)で呼び出すのでわかりやすく、複数あっても順番も関係ないため間違えにくい
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
end
