module SessionsHelper
  #渡されたユーザーでログイン
  def log_in(user)
    session[:user_id] = user.id
  end

  #ユーザーのセッションを永続的にする
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  #渡されたユーザーがログイン済みならtrue
  def current_user?(user)
    user == current_user
  end

  #記憶トークンcookieに対応するユーザーを返す
  def current_user#1
    if (user_id = session[:user_id])#2
      @current_user ||= User.find_by(id: user_id)#3,4
    elsif(user_id = cookies.signed[:user_id])
      user = User.find_by(id:user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  #ユーザーがログインしていればtrueその他ならfalseを返す
  def logged_in?
    !current_user.nil?
  end

  #永続的セッションを破棄する
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # 現在のユーザーをログアウトする
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  #記憶したURL(もしくはデフォルト値)にリダイレクト
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url]||default)
     session.delete(:forwarding_url)
  end

  #アクセスしようとしたURLを覚えておく
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end

#1.@current_userがないときは初期化し、以降は@current_userを返す
#2.代入した結果、左辺がfalse,nil以外ならtrue
#3||=は左辺がnil,falseなら右辺を代入する or equals の短縮形演算子
#4findメソッドは見つからないとエラーだがfind_byメソッドは見つからないとnilを返す
