class Relationship < ApplicationRecord
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"
  #validates :follower_id, presence: true 無くても自動でvalidateしてくれる
  #validates :followed_id, presence: true
end
