class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by(email: params[:email])#1
    if user && !user.activated && user.authenticated?(:activation, params[:id])#2
      user.activate
      log_in user
      flash[:success] = "Account activated"
      redirect_to user
    else
      flash[:dengar] = "Invaild activation link"
      redirect_to root_url
    end
  end
end

#1.params[:email]はクエリパラメータでurlの後ろに?で組み込まれたemailを抽出
#2.params[:id]はurlの中のuserごとにダイジェスト化した一意のトークンを抽出
#ルーティングでaccount_activations/[:id]/editのurlはこのアクションへ繋がると設定されている
