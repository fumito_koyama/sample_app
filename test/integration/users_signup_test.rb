require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
  end

  test "invalid signup information" do
    get signup_path #ユーザー登録ページに行き
    assert_no_difference 'User.count' do#処理を行う前と後の引数の値を比較し同じだったらtrue(第二引数省略の場合、+1が期待値となるため)
      post users_path, params: { user: { name:  "", #通らない値を送る
                                         email: "user@invalid",
                                         password:              "foo",
                                         password_confirmation: "bar" } }
    end
    assert_template 'users/new' #ユーザー登録のビューが表示されているか
    assert_select 'div#error_explanation' #htmlにそのidがあるか(演習7.3.4.1)
    assert_select 'div.alert' #htmlにそのクラスがあるか(演習7.3.4.1)
    assert_select 'form[action="/signup"]'#htmlのformの宛先があっているか(演習7.3.4.4)
  end

  test "valid signup information with account activation" do
    get signup_path#ページに行き
    assert_difference 'User.count', 1 do#Userが処理をする前より後のほうが1増えていたらtrue
      post users_path, params: { user: { name:  "Example User",#正規の値を入力
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    # 有効化していない状態でログインしてみる
    log_in_as(user)
    assert_not is_logged_in?
    # 有効化トークンが不正な場合
    get edit_account_activation_path("invalid token", email: user.email)
    assert_not is_logged_in?
    # トークンは正しいがメールアドレスが無効な場合
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not is_logged_in?
    # 有効化トークンが正しい場合
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?
    follow_redirect! #post送信先へ移動
    assert_template 'users/show'#ユーザープロフィールに関するほぼ全て (例えばページにアクセスしたらなんらかの理由でエラーが発生しないかどうかなど) をテスト
    assert_not flash.empty? #ハッシュの値が空ならfalse(演習7.4.4.1)
    assert is_logged_in? #ログイン状態ならパス
  end
end
