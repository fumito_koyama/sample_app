require 'test_helper'

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:michael)
    remember(@user)
  end

  test "current_user returns right user when session is nil" do
    assert_equal @user, current_user#1
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))#2
    assert_nil current_user
  end
end
#1.トークンありセッションなしの状態でcurrent_userを呼んで、ログインできるか
#2.間違ったトークンのダイジェストをデータベースに保存しcurrent_userを呼んだ場合
#@current_usernに代入されずnilとなるか
